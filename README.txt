
This module allows signing up (as per the Signup module) for multiple nodes at once. At the moment the collection/sequence of nodes to sign up for can be specified only by the Node Repeat module (which is required), and it will only signup for the currently viewed and future occurrences of a node.

It would be nice if future versions allowed specifying collections in some other way (eg from a View), but the author has no intention of implementing this (patches welcome).


Usage

When this module is enabled, when viewing a node that is part of a Node Repeat sequence, a check box is added under the normal Signup button that allows the user to choose to also signup for all future occurrences of the node. After signing up a link appears under the normal Cancel signup link for canceling for all future occurrences of this event, which when clicked takes the customer to a confirmation page. (When viewing the last node in the sequence this check box or link doesn't show up.)

After signing up or canceling for multiple nodes a configurable message is displayed to the user indicating how many signups or cancellations were performed. Email notifications (provided by Signup), are only performed for the first signup, but if enabled (in Signup settings) reminder emails should still be sent for all signups.

There's an admin page at admin/settings/signup_multiple for setting some of the labels/messages displayed to users.


(Un)known issues

This module is probably a bit rough around the edges; for example

    * I have just realised that I haven't tested for what happens if a node in a sequence is not Signup enabled,
    * it doesn't handle the situation where a future occurrence of a node has already been signed-up for very elegantly (primarily due to laziness on my part!), and
    * performance when dealing with multiple signups could be improved.

These and other issues should be addressed in future versions.